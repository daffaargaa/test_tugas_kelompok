<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\GoodsController;
use App\Http\Controllers\ItemsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', [AuthController::class, 'home'])->name('login');
Route::post('/', [AuthController::class, 'login'])->name('login');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [PagesController::class, 'home']);
    Route::get('/dashboard', [PagesController::class, 'home']);
    Route::get('/listBarang', [GoodsController::class, 'index']);
    Route::get('/detailBarang', [ItemsController::class, 'index']);
    Route::get('/detailBarang/{good}', [ItemsController::class, 'show']);
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
});


