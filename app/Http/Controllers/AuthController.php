<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function home()
    {
        return view('auth/login');
    }

    public function login(Request $req)
    {
        //dd($req->all());
        //pencarian email, kalo ada di validasi
        //$data = User::where('email', $req->email)->firstOrFail();
        // if ($data){
        //     if(Hash::check($req->password, $data->password)) {
        //         session(['berhasil_login' => true]);
        //         return redirect('/dashboard');
        //     }
        // }
        if (Auth::attempt(['email' => $req->email, 'password' => $req->password])) {
            return redirect('/dashboard');
        }
        return redirect('/')->with('message', 'Email atau Password anda salah!!');
    }

    public function logout(Request $req)
    {
        //$req->session()->flush();
        Auth::logout();
        return redirect('/');
    }
}
