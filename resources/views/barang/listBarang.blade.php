@extends('layout/template')

@section('title', 'List Barang')

@section('container')
    <table>
        <tr>
            <td><a href="" class="btn btn-primary" role="button">Cari</a></td>
            <td><a href="" class="btn btn-success" role="button">Tambah</a></td>
            <td><a href="" class="btn btn-danger" role="button">Hapus</a></td>
        </tr>
    </table>
    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">ID_Barang</th>
                <th scope="col">Nama</th>
                <th scope="col">Jumlah</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($goods as $good)
            <tr>
                <th scope="row">{{$loop -> iteration}}</th>
                <td>{{$good -> goods_id}}</td>
                <td>{{$good -> name}}</td>
                <td>{{$good -> stock}}</td>
                <td>
                    <a href="/detailBarang/{{$good->goods_id}}" class="badge badge-primary">Detail</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection