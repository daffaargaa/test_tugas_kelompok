@extends('layout/template')

@section('title', 'Detail Barang')

@section('container')
    <table>
        <tr>
            <td><a href="" class="btn btn-primary" role="button">Cari</a></td>
            <td><a href="" class="btn btn-success" role="button">Tambah</a></td>
            <td><a href="" class="btn btn-danger" role="button">Hapus</a></td>
        </tr>
    </table>
    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">ID_Barang</th>
                <th scope="col">ID_Item</th>
                <th scope="col">Nama</th>
                <th scope="col">Harga</th>
                <th scope="col">Jumlah</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($items as $item)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $item->goods_id }}</td>
                    <td>{{ $item->items_id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->price }}</td>
                    <td>{{ $item->stock }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
